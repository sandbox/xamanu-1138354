<?php

/**
 * Implementation of hook_node_info().
 */
function timeline_examples_node_info() {
  $items = array(
    'event_type_1' => array(
      'name' => t('Event type 1'),
      'module' => 'features',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '1',
      'body_label' => t('Body'),
      'min_word_count' => '0',
      'help' => '',
    ),
    'event_type_2' => array(
      'name' => t('Event type 2'),
      'module' => 'features',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '1',
      'body_label' => t('Body'),
      'min_word_count' => '0',
      'help' => '',
    ),
    'event_type_3' => array(
      'name' => t('Event type 3'),
      'module' => 'features',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '1',
      'body_label' => t('Body'),
      'min_word_count' => '0',
      'help' => '',
    ),
  );
  return $items;
}

/**
 * Implementation of hook_views_api().
 */
function timeline_examples_views_api() {
  return array(
    'api' => '3.0-alpha1',
  );
}
